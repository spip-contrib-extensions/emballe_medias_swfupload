<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/emballe_medias_swfupload
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// S
	'swfupload_description' => 'SWFupload.
_ Ce plugin  fournit la librairie SWFupload et une fonction de vérification de type mime qui doit être appliquée à la suite de l’upload.',
	'swfupload_slogan' => 'SWFupload'
);
