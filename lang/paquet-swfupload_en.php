<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/swfupload-paquet-xml-emballe_medias_swfupload?lang_cible=en
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// S
	'swfupload_description' => 'SWFupload.
_ This plugin provides the SWFUpload library and a function which check the mime type to be applied after the upload.',
	'swfupload_slogan' => 'SWFupload'
);
